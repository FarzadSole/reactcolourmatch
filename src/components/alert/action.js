import * as CONSTANT from './constant';

let messageId = 1;

export const init = () => {
	return {
		type: CONSTANT.ALERT_INIT
	}
};

export const add = (obj) => {
	return {
		type: CONSTANT.ALERT_ADD,
		data: {
			id: messageId++,
			type: obj.type,
			message: obj.message,
			hide: typeof obj.hide !== 'undefined' ? obj.hide : false,
			popup: typeof obj.popup !== 'undefined' ? obj.popup : false
		}
	};
};

export const show = (obj) => {
	return {
		type: CONSTANT.ALERT_SHOW,
		data: {
			id: messageId++,
			message: typeof obj !== 'undefined' ? obj.message : null
		}
	}
};

export const hide = (id) => {
	return {
		type: CONSTANT.ALERT_HIDE,
		data: {
			id
		}
	}
};