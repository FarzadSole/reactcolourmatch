export const ALERT_INIT = 'INIT';
export const ALERT_ADD = 'ALERT_ADD';
export const ALERT_SHOW = 'SHOW';
export const ALERT_HIDE = 'HIDE';
export const ALERT_SHOWN = 'SHOWN';
export const ALERT_HID = 'HID';
