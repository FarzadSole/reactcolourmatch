import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionAlert from './action';
import './style.css';

class Alert extends Component {
	constructor(props) {
		super(props);

		this.classStringType = (type) => {
			switch (type.toUpperCase()) {
				case 'SUCCESS':
					return 'success';
				case 'ERROR':
					return 'error';
				case 'WARNING':
					return 'warning';
				default:
					return 'default';
			}
		};

		// Hide Log
		this.hide = (id) => {
			this.props.alertHide(id);
		};

	}

	render() {

		if (this.props.state.log) {
			// Loop Through All Alerts
			this.props.state.log.map((e, i) => {

				// If Alert Never Shown Before
				if (this.props.state.log[i].shown === false) {

					let classString = 'inline';
					
					// Show Alert as Popup
					if (this.props.state.log[i].popup === true) {
						classString = 'popup';
					}

					this.props.state.log[i].classString = `alert ${this.classStringType(this.props.state.log[i].type)} ${classString}`;

					// Create DOM Element
					this.props.state.log[i].element = (
						<div key={i} className={this.props.state.log[i].classString}>
							<p>{this.props.state.log[i].message}</p>
							<button onClick={() => this.hide(this.props.state.log[i].id)}>X</button>
						</div>
					);

					// Auto Hide
					if (this.props.state.log[i].hide !== false) {
						// If Value Type is Boolean, Use 3000 Milliseconds as Default Value
						// If Value Type is Number, Use the Value
						const sec = typeof this.props.state.log[i].hide === 'number' ? this.props.state.log[i].hide : 3000;
						setTimeout(() => {
							this.hide(this.props.state.log[i].id);
						}, sec)
					}
				}
			});
		}

		return (
			(this.props.state.log) ? (
				this.props.state.log.map((e, i) => (
					(this.props.state.log[i].shown === false && (
						this.props.state.log[i].element
					))
				))
			) : (
				<p>No new alerts to display.</p>
			)
		);
	}
}

const mapStateToProps = (state) => ({
	state: state.reducerAlert
});

const mapDispatchToProps = (dispatch) => ({
	alertAdd: (obj) => dispatch(actionAlert.add(obj)),
	alertHide: (id) => dispatch(actionAlert.hide(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Alert);
