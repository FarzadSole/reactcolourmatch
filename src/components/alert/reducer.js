import * as CONSTANT from './constant';
import * as utils from '../../utils';

const initialState = {
	log: []
};

export default function reducerAlert(state = initialState, action) {
	switch (action.type) {

		// Show Log
		case CONSTANT.ALERT_SHOW:
			return state;

		// Add Log
		case CONSTANT.ALERT_ADD:
			if (typeof action.data !== 'undefined') {
				// Clone
				const stateNew = Object.assign({}, state);
				stateNew.log.push({
					id: action.data.id,
					type: action.data.type,
					message: action.data.message,
					shown: false,
					hide: action.data.hide,
					popup: action.data.popup
				});
				return stateNew;
			}
			return state;

		// Hide Log
		case CONSTANT.ALERT_HIDE:
			return utils.setChildObj(state, ['log'], { id: action.data.id }, {
				shown: true
			});

		// Default
		default:
			return state;
	}
}
