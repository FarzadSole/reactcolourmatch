import {
	COUNTER_INIT,
	COUNTER_ADD,
	COUNTER_START,
	COUNTER_STOP,
	COUNTER_INCREMENT,
	COUNTER_DECREMENT,
	COUNTER_INCREMENT_SWITCH,
	COUNTER_DECREMENT_SWITCH
} from './constant';

let counterId = 1;

export const init = () => ({
	type: COUNTER_INIT
});

export const add = (obj) => ({
	type: COUNTER_ADD,
	data: {
		id: counterId++,
		name: obj.name
	}
});

export const start = (id) => ({
	type: COUNTER_START,
	id
});

export const stop = (id) => ({
	type: COUNTER_STOP,
	id
});

export const increment = (id) => ({
	type: COUNTER_INCREMENT,
	id
});

export const decrement = (id) => ({
	type: COUNTER_DECREMENT,
	id
});

export const incrementSwitch = (id) => ({
	type: COUNTER_INCREMENT_SWITCH,
	id
});

export const decrementSwitch = (id) => ({
	type: COUNTER_DECREMENT_SWITCH,
	id
});
