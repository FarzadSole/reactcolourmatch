import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCounter from './action';
import * as actionAlert from '../alert/action';
import './style.css';

class Counter extends Component {
	constructor(props) {
		super(props);

		// Temporary State
		this.state = {
			name: ''
		};

		// Update Temporary State Name
		this.handleStateName = event => {
			this.setState({ name: event.target.value });
		};

		// Add Counter
		this.add = () => {
			if (this.state.name !== '') {
				this.props.counterAdd({
					name: this.state.name
				});
				this.props.alertAdd({
					type: 'SUCCESS',
					message: `Counter ${this.state.name}, was successfully added`
				});
				// Clear Temporary State Name
				this.setState({ name: '' });
			} else {
				this.props.alertAdd({
					type: 'ERROR',
					message: 'Name is required.'
				});
			}
		};

		// Start/Pause Counter with Speicific ID
		this.start = (obj) => {
			this.props.start(obj);
			// if (!this.props.state.counting) {
			// 	this.props.start(id);
			// } else {
			// 	this.props.stop(id);
			// }
			// console.log(this.props.state.instance.filter(e => e.id === id));
			// for (let i = 0; i < this.props.state.instance.length; i++) {
			// 	if (this.props.state.instance[i].id === id) {
			// 		console.log(this.props.state.instance[i]);
			// 	}
			// }
		};
	}

	componentWillMount() {
		// this.props.init();

		// setInterval(() => {
		// 	if (this.props.state.counting) {
		// 		if (this.props.state.count === 0) {
		// 			this.props.incrementSwitch();
		// 		}

		// 		if (this.props.state.incrementing) {
		// 			this.props.increment();
		// 		} else {
		// 			this.props.decrement();
		// 		}
		// 	}
		// }, 1000);
	}

	secondsToHMS(sec) {
		const secs = Number(sec);
		const H = Math.floor(secs / 3600);
		const M = Math.floor((secs % 3600) / 60);
		const S = Math.floor(secs % 3600 % 60);
		return [H, M, S];
	}

	counterHMS(index) {
		const HMS = this.secondsToHMS(this.props.state.instance[index].count);
		return `${HMS[0]} Hrs ${HMS[1]} Min ${HMS[2]} Sec`;
	}

	// buttonStart = (id) {
	// 	if (!this.props.state.counting) {
	// 		this.props.start(id);
	// 	} else {
	// 		this.props.stop(id);
	// 	}
	// };

	// buttonSwitch(id) {
	// 	if (!this.props.state.incrementing) {
	// 		this.props.incrementSwitch(id);
	// 	} else {
	// 		this.props.decrementSwitch(id);
	// 	}
	// };

	render() {
		console.log(this.props.state.instance.length);

		// const instanceLength = typeof this.props.state.instance !== undefined ? true : false;

		return (
			<div>
				<input type="text" value={this.state.name} onChange={this.handleStateName} />
				<button onClick={() => this.add()}>{this.props.state.buttonAddText}</button>

				{
					(this.props.state.instance.length > 0 && (
						this.props.state.instance.map((e, i) => (
							<div key={i}>
								<table>
									<tbody>
										<tr>
											<td>ID</td>
											<td>{this.props.state.instance[i].id}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>{this.props.state.instance[i].name}</td>
										</tr>
										<tr>
										<td width="50%">Count</td>
											<td><span>{this.props.state.instance[i].count}</span></td>
										</tr>
										<tr>
											<td>Total</td>
											<td><span>{this.counterHMS(i)}</span></td>
										</tr>
									</tbody>
								</table>
								<button onClick={() => this.start({ id: this.props.state.instance[i].id })}>
									{this.props.state.instance[i].buttonText} Start/Pause
								</button>
								<button onClick={() => this.switch(this.props.state.instance[i].id)}>
									{this.props.state.instance[i].directionText} Switch Direction
								</button>
							</div>
						))
					))
				}
			</div>

			// <div>
			// 	<button onClick={buttonAdd}>{this.props.state.buttonAddText}</button>
			// </div>
		);
	}
}

const mapStateToProps = (state) => ({
	state: state.reducerCounter
});

const mapDispatchToProps = (dispatch) => ({
	counterInit: () => dispatch(actionCounter.init()),
	counterAdd: (obj) => dispatch(actionCounter.add(obj)),
	counterStart: (obj) => dispatch(actionCounter.start(obj)),
	counterStop: (obj) => dispatch(actionCounter.stop(obj)),
	counterIncrement: () => dispatch(actionCounter.increment()),
	counterDecrement: () => dispatch(actionCounter.decrement()),
	counterIncrementSwitch: () => dispatch(actionCounter.incrementSwitch()),
	counterDecrementSwitch: () => dispatch(actionCounter.decrementSwitch()),

	alertAdd: (obj) => dispatch(actionAlert.add(obj))
});

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
