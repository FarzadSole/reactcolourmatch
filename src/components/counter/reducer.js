import {
	COUNTER_INIT,
	COUNTER_ADD,
	COUNTER_START,
	COUNTER_STOP,
	COUNTER_INCREMENT,
	COUNTER_DECREMENT,
	COUNTER_INCREMENT_SWITCH,
	COUNTER_DECREMENT_SWITCH
} from './constant';

const initialState = {
	buttonAddText: 'Add',
	instance: []
};

export default function reducerCounter(state = initialState, action) {
	// Return an instance that matches the given id
	// This method will be used within the switch statement
	function getInstance(id) {
		console.log(`Instance ID = ${id}`);
		if (typeof id === undefined) {
			return {};
		}
		const result = state.instance.filter(el => el.id === id);
		return typeof result !== undefined ? result : {};
	}
	switch (action.type) {
		case COUNTER_INIT:
			console.log('__INIT__');
			return Object.assign({}, state, {
				buttonText: state.count > 0 ? 'Continue' : 'Start',
				directionText: state.incrementing > 0 ? 'Incrementing ▲' : '▼ Decrementing'
			});
		case COUNTER_ADD:
			console.log('__ADD__');
			console.log(action);
			return {
				buttonAddText: state.buttonAddText,
				instance: [
					...state.instance,
					{
						id: action.data.id,
						name: action.data.name,
						count: 0,
						counting: false,
						incrementing: true,
						buttonText: ' ',
						buttonClass: ' ',
						directionText: ' '
					}
				]
			};
		case COUNTER_INCREMENT:
			console.log('__INCREMENT__');
			// if (action.id === state.instance.id) {
				return Object.assign({}, state.instance.filter(e => e.id === action.id), {
					// count: state.count + 1
					count: state.instance.filter(e => e.id === action.id).count + 1
				});
			// }
			// break;
		case COUNTER_DECREMENT:
			console.log('__DECREMENT__');
			return Object.assign({}, state.instance.filter(e => e.id === action.id), {
				// count: state.count - 1
				count: state.instance.filter(e => e.id === action.id).count - 1
			});
		case COUNTER_START:
			console.log('__START__');
			return Object.assign({}, getInstance(action.id), {
				counting: true,
				buttonText: 'Pause',
				buttonClass: 'warning'
			});
			// const instance = state.instance.filter(function(el) { return el.id === action.id });
			// console.log(instance)
			// return state;
			// if (action.id === state.instance.id) {
			// 	return Object.assign({}, state.instance.filter(e => e.id === action.id), {
			// 		counting: true,
			// 		buttonText: 'Pause',
			// 		buttonClass: 'warning'
			// 	});
			// }
			// break;
		case COUNTER_STOP:
			console.log('__STOP__');
			// return Object.assign({}, state.instance.filter(e => e.id === action.id), {
			// 	counting: false,
			// 	buttonText: 'Continue',
			// 	buttonClass: 'success'
			// });
			break;
		case COUNTER_INCREMENT_SWITCH:
			console.log('__INCREMENT_SWITCH__');
			return Object.assign({}, state.instance.filter(e => e.id === action.id), {
				incrementing: true,
				directionText: 'Incrementing ▲'
			});
		case COUNTER_DECREMENT_SWITCH:
			console.log('__DECREMENT_SWITCH__');
			return Object.assign({}, state.instance.filter(e => e.id === action.id), {
				incrementing: false,
				directionText: 'Decrementing ▼'
			});
		default:
			return state;
	}
}
