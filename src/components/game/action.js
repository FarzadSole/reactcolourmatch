import * as CONSTANT from './constant';

let gameId = 1;

export const init = () => {
	return {
		type: CONSTANT.GAME_INIT
	}
};

export const start = (obj) => {
	return {
		type: CONSTANT.GAME_START,
		data: {
			id: gameId++,
			name: obj.name,
			cellH: obj.cellH,
			cellV: obj.cellV,
			cellData: obj.cellData,
			time: obj.time
		}
	}
};

// To end a game we must know its id and result
// So we would know if it was successful or unsuccessful game
// If time is up, this action gets dispatched automatically
export const end = (obj) => {
	return {
		type: CONSTANT.GAME_END,
		data: {
			id: obj.id,
			result: obj.result,
			time: obj.time
		}
	}
};

export const successful = (obj) => {
	return {
		type: CONSTANT.GAME_SUCCESSFUL,
		data: {
			id: obj.id
		}
	}
};

export const timeout = () => {
	return {
		type: CONSTANT.GAME_TIMEOUT
	}
};

export const counter = (obj) => {
	return {
		type: CONSTANT.GAME_COUNTER,
		data: {
			id: obj.id,
			time: obj.time
		}
	}
};

export const counterDecrement = (obj) => {
	return {
		type: CONSTANT.GAME_COUNTER_DECREMENT,
		data: {
			id: obj.id,
			time: obj.time
		}
	}
};