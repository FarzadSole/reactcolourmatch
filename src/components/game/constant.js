export const GAME_INIT = 'GAME_INIT';
export const GAME_START = 'GAME_START';
export const GAME_END = 'GAME_END';
export const GAME_SUCCESSFUL = 'GAME_SUCCESSFUL';
export const GAME_TIMEOUT = 'GAME_TIMEOUT';
export const GAME_COUNTER = 'GAME_COUNTER';
export const GAME_COUNTER_DECREMENT = 'GAME_COUNTER_DECREMENT';
