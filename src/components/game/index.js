import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import * as actionAlert from '../alert/action';
import * as actionGame from './action';
import * as utils from '../../utils';
import './style.css';

class Game extends Component {
	constructor(props) {
		super(props);

		// Language String
		this.languageString = {
			NAME: 'Name',
			CELL_H: 'Horizontal Cells',
			CELL_V: 'Vertical Cells',
			TIME: 'Allowed Time (Seconds)',
			START: 'Start Game'
		}

		// Temporary State
		this.state = {
			name: '',
			cellH: 5,
			cellV: 5,
			time: 300,
			cellData: [],
			formValid: false
		};

		// Update Temporary State
		// Name
		this.handleStateName = event => {
			this.setState({ name: event.target.value });
		};
		// Horizontal Cells
		this.handleStateCellH = event => {
			this.setState({ cellH: event.target.value });
		};
		// Vertical Cells
		this.handleStateCellV = event => {
			this.setState({ cellV: event.target.value });
		};
		// Time
		this.handleStateTime = event => {
			this.setState({ time: event.target.value });
		};
		// Form Valid
		this.handleStateFormValidate = () => {
			const { name, cellH, cellV, time } = this.state;
			if (name !== '' && name.length >= 3 && cellH !== '' && cellV !== '' && time !== '') {
				// console.log('__FORM_VALID__')
				this.setState({ formValid: true });
			} else {
				// console.log('____FORM_INVALID____')
				this.setState({ formValid: false });
			}
		};

		// Random Cell, A or B
		this.generateCellData = (total) => {
			const name = ['a', 'b'];
			const result = [];
			for (let i = 0; i < total; i++) {
				const index = Math.floor(Math.random() * 2);
				result.push(name[index]);
			}
			return result;
		}

		// Start Game
		this.start = () => {
			// Promise
			axios.all([this.handleStateFormValidate()])
				.then(axios.spread(() => {

					// Check State
					if (!this.state.formValid) {
						// Add Error Message
						this.props.alertAdd({
							type: 'ERROR',
							message: `Please fill in all the fields.`,
							hide: true,
							// popup: true
						});
					} else {
						// Generate Cell Data
						const cellTotal = this.state.cellH * this.state.cellV;
						this.setState({ cellData: this.generateCellData(cellTotal) });

						//
						this.props.gameStart({
							name: this.state.name,
							cellH: this.state.cellH,
							cellV: this.state.cellV,
							cellData: this.state.cellData,
							time: this.state.time
						});

						// Add Success Message
						this.props.alertAdd({
							type: 'SUCCESS',
							message: `Please wait while we are preparing the game for you.`,
							hide: true,
							// popup: true
						});

						this.props.state.game.map((e, i) => {
							if (this.props.state.game[i].started) {

								
								// Counter Interval
								this.counterInterval = setInterval(() => {

									// Time Left
									if (this.props.state.game[i].time > 0) {

										// Decrementing the Time
										this.props.gameCounterDecrement({
											id: this.props.state.game[i].id,
											time: this.props.state.game[i].time
										});
									}
									
									// No Time Left
									else if (this.props.state.game[i].time <= 0) {

										this.setState({
											formValid: false
										});
										// Add Error Message
										this.props.alertAdd({
											type: 'ERROR',
											message: `Oops your time is over!`,
											hide: true,
											// popup: true
										});

										// Clear Counter Interval
										console.log('Clear interval')
										clearInterval(this.counterInterval);
									}

								}, 1000);

							}
						});
					}
				}));
		};
		//

		this.renderCounter = () => {
			return this.props.state.game.map((e, i) => {
				if (this.props.state.game[i].started) {
					utils.toHMS((this.props.state.game[i].time))
				}
			});
		};

		this.renderCell = () => {
			
			// Each Game
			// this.props.state.game.map((_e, _i) => {
				// console.log(this.props.state.game[_i].name)
				let row = [];
				let count = 0;
				for (let i = 0; i < this.state.cellH; i++) {
					const cell = [];
					for (let j = 0; j < this.state.cellV; j++) {

						// 
						const cellDataIndex = count;
						const cellData = this.state.cellData[cellDataIndex];

						// Push Each Cell to Its Row
						cell.push(<div key={j} className={`cell cell-${cellDataIndex} cell-${cellData}`} style={{
							width: `${100 / this.state.cellV}%`,
							height: `${80}px`
						}} onClick={() => {

							const inverse = cellData === 'a' ? 'b' : 'a';

							// Clone cellData
							const cellDataNew = Object.assign([], this.state.cellData);
							
							// Update Index 0
							cellDataNew.splice(cellDataIndex, 1, inverse);

							// Set New State
							const handleStateCellData = () => {
								this.setState({
									cellData: cellDataNew
								});
							};
							

							axios.all([handleStateCellData()])
								.then(axios.spread(() => {
									// Listen to Cell Data
									// If all Values are Same it is a Win
									if (utils.hasDuplicates(this.state.cellData)) {
										// Add Alert
										this.props.alertAdd({
											type: 'SUCCESS',
											message: `You Won :)`,
											// hide: 1000,
											popup: true
										});
										// this.props.gameSuccessful
									} else {
									}
								}));
							

						}}>{`${i} : ${cellDataIndex} - ${cellData}`.toUpperCase()}</div>);
						count++;
					}
					row.push(<div key={i} className={`row row-${i}`}>{cell}</div>);
				}
				return row;
			// });
			
		};
	}



	render() {

		return (
			<div className="game">
				{
					(!this.state.formValid) ? (
						<div className="form">
							<div className="row">
								<label className="column-4">{this.languageString.NAME}:</label>
								<input type="text" value={this.state.name} onChange={this.handleStateName} />
							</div>
							<div className="row">
								<label className="column-4">{this.languageString.CELL_H}:</label>
								<input type="text" value={this.state.cellH} onChange={this.handleStateCellH} />
							</div>
							<div className="row">
								<label className="column-4">{this.languageString.CELL_V}:</label>
								<input type="text" value={this.state.cellV} onChange={this.handleStateCellV} />
							</div>
							<div className="row">
								<label className="column-4">{this.languageString.TIME}:</label>
								<input type="text" value={this.state.time} onChange={this.handleStateTime} />
							</div>
							<div className="row">
								<label className="column-4"></label>
								<button className="button" type="text" value={this.state.name} onClick={() => this.start()}>{this.languageString.START}</button>
							</div>
						</div>
					) : (
						<div className="grid">
							{
								`Counter: ${this.renderCounter()}`
							}
							{
								this.renderCell()
							}
						</div>
					)
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	state: state.reducerGame
});

const mapDispatchToProps = (dispatch) => ({
	// gameInit: () => dispatch(actionGame.init()),
	gameStart: (obj) => dispatch(actionGame.start(obj)),
	gameEnd: (obj) => dispatch(actionGame.end(obj)),
	gameSuccessful: (obj) => dispatch(actionGame.successful(obj)),
	gameCounter: (obj) => dispatch(actionGame.counter(obj)),
	gameCounterDecrement: (obj) => dispatch(actionGame.counterDecrement(obj)),

	alertAdd: (obj) => dispatch(actionAlert.add(obj)),
	alertHide: (obj) => dispatch(actionAlert.hide(obj))
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);
