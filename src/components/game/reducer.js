import * as CONSTANT from './constant';
import { setChildObj } from '../../utils';

const initialState = {
	game: []
};

export default function reducerGame(state = initialState, action) {
	switch (action.type) {

		// Init
		case CONSTANT.GAME_INIT:
			return state;
		
		// Start
		case CONSTANT.GAME_START:
			if (typeof action.data !== 'undefined') {
				const stateNew = Object.assign({}, state);
				stateNew.game.push({
					id: action.data.id,
					name: action.data.name,
					cellH: action.data.cellH,
					cellV: action.data.cellV,
					cellData: action.data.cellData,
					time: action.data.time,
					started: true,
					ended: false,
					successful: null
				});
				return stateNew;
			}
			return state;

		// End
		case CONSTANT.GAME_END:
			return setChildObj(state, ['game'], { id: action.data.id }, {
				ended: true
			});

		// Successful
		case CONSTANT.GAME_SUCCESSFUL:
			return setChildObj(state, ['game'], { id: action.data.id }, {
				successful: true
			});

		// Timeout
		case CONSTANT.GAME_TIMEOUT:
			return setChildObj(state, ['game'], { id: action.data.id }, {
				time: 0
			});

		// Counter
		case CONSTANT.GAME_COUNTER:
			return setChildObj(state, ['game'], { id: action.data.id }, {
				time: action.data.time
			});

		// Counter Decrement
		case CONSTANT.GAME_COUNTER_DECREMENT:
			return setChildObj(state, ['game'], { id: action.data.id }, {
				time: action.data.time - 1
			});

		// Default
		default:
			return state;
	}
}
