import * as CONSTANT from './constant';

export const init = () => ({
	type: CONSTANT.SCREEN_INIT
});
