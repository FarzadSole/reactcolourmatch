import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionScreen from './action';
import * as actionAlert from '../alert/action';
import { Game } from '../../components';

class Screen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Game />
		);
	}
}

const mapStateToProps = (state) => ({
	state: state.reducerScreen
});

const mapDispatchToProps = (dispatch) => ({
	screenInit: () => dispatch(actionScreen.init()),

	alertAdd: (obj) => dispatch(actionAlert.add(obj))
});

export default connect(mapStateToProps, mapDispatchToProps)(Screen);
