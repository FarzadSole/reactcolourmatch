import * as CONSTANT from './constant';

const initialState = {};

export default function reducerProfile(state = initialState, action) {
	switch (action.type) {
		case CONSTANT.SCREEN_INIT:
			return state;
		default:
			return state;
	}
}
