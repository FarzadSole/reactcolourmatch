import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import { Screen, Alert } from './components';
import './style/index.css';
import registerServiceWorker from './registerServiceWorker';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<div className="container">
					<Alert />
					<Screen />
				</div>
			</Provider>
		);
	}
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
