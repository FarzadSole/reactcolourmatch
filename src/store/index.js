import { combineReducers, createStore } from 'redux';
import * as reducers from './reducers';

const defaultReducers = combineReducers(reducers);
const store = createStore(
	defaultReducers
);

store.subscribe(() => {
	console.log('Store Updated', store.getState());
});
export default store;
