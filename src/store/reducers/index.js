import reducerAlert from '../../components/alert/reducer';
import reducerScreen from '../../components/screen/reducer';
import reducerGame from '../../components/game/reducer';

export {
	reducerAlert,
	reducerScreen,
	reducerGame
};
