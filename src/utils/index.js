// Update Child Object
function setChildObj(obj, child, keyValue, source) {
    const objClone = Object.assign({}, obj);
    let target = null;
    objClone[child].filter(e => {
        // As Object
        const key = e[Object.keys(keyValue)];
        const value = keyValue[Object.keys(keyValue)];
        // As Array
        // const key = e[keyValue[0]];
        // const value = keyValue[1];
        if (key === value) {
            target = e;
        }
        return false;
    });
    // console.log(target);
    const objUpdate = Object.assign(target, source);
    const objChild = obj[child];
    objChild.map((e, i) => {
        if (e.id === objUpdate.id) {
            objChild[i] = objUpdate;
        }
        return false;
    });
    return objClone;
}

// Seconds to H:M:S
function toHMS(sec) {
    const secs = Number(sec);
    const H = Math.floor(secs / 3600);
    const M = Math.floor((secs % 3600) / 60);
    const S = Math.floor(secs % 3600 % 60);
    return [H, M, S];
}

// Check Array Values for Any Duplicates
function hasDuplicates(array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] !== array[0]) {
            return false;
        }
    }
    return true;
}

export {
    setChildObj,
    toHMS,
    hasDuplicates
};